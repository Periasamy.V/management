import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css'],
})
export class CustomerComponent {
  ngOnInit() {}
  constructor(private http: HttpClient) {}
  onCreatePost(postData: {
    fname: string;
    sname: string;
    textarea: string;
    dob: string;
  }) {
    this.http
      .post(
        'https://managementdatabase-93085-default-rtdb.firebaseio.com/customer.json ',
        postData
      )
      .subscribe((responseData) => {
        console.table(responseData);
      });
  }
  // get........................
  data:any;
  
  get() {
    this.http
      .get(
        'https://managementdatabase-93085-default-rtdb.firebaseio.com/customer.json',
        
      )
      .subscribe((responseData) => {
        this.data=responseData;
      });

    }  
 
    
// delete..................
delete() {
  this.http
    .delete(
      'https://managementdatabase-93085-default-rtdb.firebaseio.com/customer.json '

    )
    .subscribe((responseData) => {
      
    });
}
// age................................

  date = Date();
  curDate: number = 2023;
  age: any;
  convert() {
    this.date = this.date.slice(0, 4);
    this.age = this.curDate - Number(this.date);
  }

  openDialog(){
    prompt("Your Details saved on data base ThankYou for coming.....!");
  }
}
