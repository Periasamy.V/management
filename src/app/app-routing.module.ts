import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManagerComponent } from './manager/manager.component';
import { EmployeeComponent } from './employee/employee.component';
import { CustomerComponent } from './customer/customer.component';
import { LogInComponent } from './log-in/log-in.component';
import { LogComponent } from './log/log.component';

const routes: Routes = [
  {path:'manager',component:ManagerComponent},
  {path:'employee',component:EmployeeComponent},
  {path:'customer',component:CustomerComponent},
  {path:'log-in',component:LogInComponent},
  {path:'log',component:LogComponent},
  // {path:'',component:ManagerComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


