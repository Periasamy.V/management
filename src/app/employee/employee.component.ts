import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent {
  formatLabel(value: number): string {
    if (value >= 1) {
      return Math.round(value / 1) + 'y';
    }

    return `${value}`;
  }

  ngOnInit() {}
  constructor(private http: HttpClient) {}
  onCreatePost(postData: { fname: string; sname: string ; textarea:string ; dob:string ;slider:string;gender:string }) {
    
    this.http
      .post(
        'https://managementdatabase-93085-default-rtdb.firebaseio.com/employee.json ',
        postData
      )
      .subscribe((responseData) => {
        console.table(responseData);
      });
  }
  // get...........................
  data:any;
  roles:any;
  get() {
    this.http
      .get(
        'https://managementdatabase-93085-default-rtdb.firebaseio.com/employee.json ',
        
      )
      .subscribe((responseData) => {
        this.data=responseData;
      });

    }  

  role() {
    this.http
      .get(
        'https://managementdatabase-93085-default-rtdb.firebaseio.com/employeeRole/role.json ',
        
      )
      .subscribe((responseData) => {
        this.roles=responseData;
      });

    }  
    // delete................
    delete() {
      this.http
        .delete(
          'https://managementdatabase-93085-default-rtdb.firebaseio.com/employee.json '
  
        )
        .subscribe((responseData) => {
          
        });
    }

  // age.........................
  date = Date();
  curDate: number = 2023;
  age: any;
convert() {
  this.date = this.date.slice(0, 4);
  this.age = this.curDate - Number(this.date);
}
//  /..........................................
  openDialog(){
    prompt("Your Details saved on data base ThankYou for coming.....!");
  }
}
