import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import{MatDialog} from '@angular/material/dialog';
// import { DialogComponent } from '../dialog/dialog.component';
@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css'],
})
export class ManagerComponent {
  ngOnInit() {}
  constructor(private http: HttpClient) {}
  onCreatePost(postData: {
    fname: string;
    sname: string;
    textarea: string;
    result: string;
    dob: string;
  }) {
    this.http
      .post(
        'https://managementdatabase-93085-default-rtdb.firebaseio.com/manager.json ',
        postData
      )
      .subscribe((responseData) => {
        console.table(responseData);
      });
  }

  // get......................
  data: any;
  roles: any;
  get() {
    this.http
      .get(
        'https://managementdatabase-93085-default-rtdb.firebaseio.com/manager.json'
      )
      .subscribe((responseData) => {
        this.data = responseData;
      });
  }
  role() {
    this.http
      .get(
        'https://managementdatabase-93085-default-rtdb.firebaseio.com/managerRole/role.json '

      )
      .subscribe((responseData) => {
        this.roles = responseData;
      });
  }
  // delete
  delete() {
    this.http
      .delete(
        'https://managementdatabase-93085-default-rtdb.firebaseio.com/manager.json '

      )
      .subscribe((responseData) => {
        
      });
  }

  // age calculation
  date = Date();
  curDate: number = 2023;
  age: any;
  convert() {
    this.date = this.date.slice(0, 4);
    this.age = this.curDate - Number(this.date);
  }
  // popup=false;
  // openDialog(){
  //   this.matDialog.open(DialogComponent);
  // }
  // public matDialog:MatDialog
  openDialog() {
    prompt('Your Details saved on data base ThankYou for coming.....!');
  }
}
